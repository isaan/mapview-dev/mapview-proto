# mapview

## setup
```
curl -sSL https://gitlab.com/isaan/mapview/raw/master/server.sh > server.sh
bash server.sh
```

### Deploy
```
cd ~/mapview
bash deploy.sh
```

### Run your tests
```
npm run test
```
