echo "Domain:"
read domain

sudo apt update
sudo apt upgrade -y
sudo apt install nginx -y
sudo systemctl start nginx
sudo systemctl enable nginx
sudo apt install nodejs npm -y

ip="$( wget -q -O - 'http://169.254.169.254/latest/meta-data/local-ipv4' )"
echo "server {
    listen 80;
    server_name $domain;
    location / {
        proxy_pass http://$ip:5000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
    }
}" | sudo tee /etc/nginx/sites-available/default

sudo /etc/init.d/nginx reload

cd
git clone https://gitlab.com/isaan/mapview.git
cd ~/mapview

npm install
sudo npm install -g @vue/cli
sudo npm install -g serve

sudo apt install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt update
sudo apt install python-certbot-nginx -y

sudo certbot --nginx