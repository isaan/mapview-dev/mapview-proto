import * as Mapbox from 'mapbox-gl';
import { COLORS } from './enums';
import { Point } from './point';

export class ImageOverlay {
    public _id: string = '';
    public _source: object;
    public image: string;
    public point1: Point;
    public point2: Point;
    public point3: Point;
    public opacity: number = 0.5;

    constructor(map: Mapbox.Map, image: string, coord1: Mapbox.LngLatLike, coord2: Mapbox.LngLatLike, coord3: Mapbox.LngLatLike, fitBounds: boolean) {
        this._id = String(Math.random());
        this.image = image;

        this.point1 = new Point(coord1);
        this.point1._icon.setFill(COLORS.BLACK).setSize(12);

        this.point3 = new Point(coord2);
        this.point3._icon.setFill(COLORS.BLACK).setSize(12);

        this.point2 = new Point(coord3);
        this.point2._icon.setFill(COLORS.BLACK).setSize(12);

        this._source = {
            type: 'image',
            url: this.image,
            coordinates: [
                [ this.point1._mapboxMarker.getLngLat().lng, this.point1._mapboxMarker.getLngLat().lat ],
                [ this.point2._mapboxMarker.getLngLat().lng, this.point2._mapboxMarker.getLngLat().lat ],
                [ this.point3._mapboxMarker.getLngLat().lng, this.point3._mapboxMarker.getLngLat().lat ],
                [ this.point1._mapboxMarker.getLngLat().lng, this.point3._mapboxMarker.getLngLat().lat ],
            ],
        };

        const IMAGE = new Image();
        IMAGE.src = this.image;
        IMAGE.onload = (event: Event) => {
            this.point1._mapboxMarker.addTo(map);
            this.point2._mapboxMarker.addTo(map);
            this.point3._mapboxMarker.addTo(map);

            map.addLayer(this.genLayer());
            if (fitBounds) {
                this.fitBounds(map);
            }
        };

        this.point1._icon.svg.addEventListener('click', (event: Event) => {
            this.fitBounds(map);
        });
        this.point2._icon.svg.addEventListener('click', (event: Event) => {
            this.fitBounds(map);
        });
        this.point3._icon.svg.addEventListener('click', (event: Event) => {
            this.fitBounds(map);
        });

        // map.on('click', this._id, () => {
        //     console.log('test');
        //     this.fitBounds(map);
        // });

        // map.on('mouseenter', this._id, () => {
        //     map.getCanvas().style.cursor = 'pointer';
        // });

        // map.on('mouseleave', this._id, () => {
        //     map.getCanvas().style.cursor = '';
        // });
    }

    public clean(map: Mapbox.Map): this {
        map.removeLayer(this._id);
        map.removeSource(this._id);
        this.point1._mapboxMarker.remove();
        this.point2._mapboxMarker.remove();
        this.point3._mapboxMarker.remove();

        return this;
    }

    public fitBounds(map: Mapbox.Map): void {
        map.fitBounds([[
            this.point1._mapboxMarker.getLngLat().lng,
            this.point1._mapboxMarker.getLngLat().lat,
        ], [
            this.point3._mapboxMarker.getLngLat().lng,
            this.point3._mapboxMarker.getLngLat().lat,
        ]], { padding: 200 });
    }

    public genLayer(): Mapbox.Layer {
        return {
            id: this._id,
            paint: {
                'raster-fade-duration': 0,
                'raster-opacity': this.opacity,
            },
            source: (this._source as Mapbox.ImageSource),
            type: 'raster',
        };
    }

    public toggle(map: Mapbox.Map): this {
        if (map.getLayer(this._id)) {
            map.removeLayer(this._id);
            map.removeSource(this._id);
        } else {
            map.addLayer(this.genLayer());
        }
        this.point1._icon.toggle();
        this.point2._icon.toggle();
        this.point3._icon.toggle();

        return this;
    }
}
