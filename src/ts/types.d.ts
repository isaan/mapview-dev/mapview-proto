declare module 'bowser';

interface Buttons {
    [key: string]: number;
}

interface Colors {
    [key: string]: string;
}

interface F25Values {
    kind: 'F25Values';
    d0: number;
    d200: number;
    d300: number;
    d450: number;
    d600: number;
    d900: number;
    d1200: number;
    d1500: number;
    d1800: number;
}

interface Icon {
    kind: 'icon',
    value: string,
}
interface Icons {
    [key: string]: Icon;
}

interface KindDate {
    name: 'date';
    type: 'date' | 'string';
}

interface KindEquation {
    name: 'equation';
    type: 'f25';
}

interface KindTemperature {
    name: 'temperature';
    type: 'celsius' | 'fahrenheit';
}

interface KindText {
    name: 'text';
    type: 'string' | 'longString';
}

interface Input {
    kind: KindDate | KindEquation | KindTemperature | KindText;
    label: string;
    value: string | number;
}

interface Limit {
    color: string,
    value: number,
}

interface Styles {
    [key: string]: string;
}

// type LimitType = 'high' | 'low';
type Values = F25Values | undefined;
