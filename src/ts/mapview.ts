import * as Mapbox from 'mapbox-gl';
import { STYLES } from './enums';
import { Folder } from './folder';
import { Folders } from './folders';
import { ImageOverlays } from './imageOverlays';
import { Line } from './line';
import { Path } from './path';
import { Point } from './point';

export class Mapview {
    public _focused: Folder | Folders | Path | Point;
    public _mapboxMap: Mapbox.Map | undefined;
    public folders: Folders = new Folders();
    public images: ImageOverlays = new ImageOverlays();
    public mapCenter: Mapbox.LngLatLike = [0, 44];
    public mapStyle: string = Object.keys(STYLES)[0];
    public mapZoom: number = 3;

    constructor() {
        (Mapbox as any).accessToken = 'pk.eyJ1Ijoia2dhd2xpayIsImEiOiJjam1nMHE0Z2kwaTMzM3FwYTc0eDd1N2g0In0.eAfnfMMVkmrAiwj5RdUKYw';
        this._focused = this.folders;
    }

    public addFolder(folder: Folder, fitBounds: boolean): this {
        if (this._mapboxMap) {
            this.folders.addFolder(this._mapboxMap, folder);
            if (fitBounds) {
                this._focused = folder.focus(this._mapboxMap);
                this.fitBounds();
            } else {
                this.unfocus();
            }
        }
        this.autoscroll();

        folder.paths.forEach((path: Path) => {
            path.points.forEach((point: Point) => {
                if (this._mapboxMap) {
                    point._mapboxMarker.addTo(this._mapboxMap);
                    point._icon.svg.addEventListener('click', (event: Event) => {
                        this.focus(point);
                        event.stopPropagation();
                    }, false);
                }
            });
            // map.on('click', path._line._id, () => {
            //     setTimeout(() => {
            //         console.log(path._line._id);
            //         justClickedOnLayer = true;
            //         this.focus(map, path._line);
            //     }, 50);
            // });
        });
        return this;
    }

    public clean(): this {
        if (this._mapboxMap) {
            this.folders.clean(this._mapboxMap);
            this.images.clean(this._mapboxMap);
        }
        return this;
    }

    public init(): this {
        this._mapboxMap = new Mapbox.Map({
            center: this.mapCenter,
            container: 'map',
            preserveDrawingBuffer: true,
            style: STYLES[(this.mapStyle as any)],
            zoom: this.mapZoom,
        }).addControl(new Mapbox.NavigationControl(), 'top-left');
        this._mapboxMap.doubleClickZoom.disable();
        this._mapboxMap.on('click', (event: any) => {
            this._focused = this.folders;
            this.unfocus();
        });

        this.folders.init(this._mapboxMap);
        this.images.init(this._mapboxMap);

        return this;
    }

    public selectArea(): this {
        return this;
    }

    public setMapStyle(style: string): this {
        if (this._mapboxMap) {
            this.mapStyle = style;
            this._mapboxMap.setStyle(STYLES[(this.mapStyle as any)]);
        }
        return this;
    }

    // ---
    // PRIVATE
    // ---

    private autoscroll(): void {
        setTimeout(() => {
            const DIV: HTMLElement | null = document.getElementById('live');
            if (DIV !== null) {
                DIV.scrollIntoView({ block: 'start', inline: 'nearest', behavior: 'smooth' });
            }
        }, 50);
    }

    private fitBounds(): void {
        if (this._mapboxMap) {
            if (this._focused instanceof Folder || this._focused instanceof Path) {
                const BOUNDS: Mapbox.LngLatBounds = new Mapbox.LngLatBounds();
                if (this._focused instanceof Folder) {
                    this._focused.paths.forEach((path: Path) => {
                        path.points.forEach((point: Point) => {
                            BOUNDS.extend(point._mapboxMarker.getLngLat());
                        });
                    });
                } else if (this._focused instanceof Path) {
                    this._focused.points.forEach((point: Point) => {
                        BOUNDS.extend(point._mapboxMarker.getLngLat());
                    });
                }

                this._mapboxMap.fitBounds(BOUNDS, { padding: 100 });
            // } else if (this._focused instanceof Point) {
            //     let zoom: number = this._mapboxMap.getZoom();
            //     if (zoom < 18) {
            //         zoom = 18;
            //     }
            //     this._mapboxMap.flyTo({
            //         center: this._focused._mapboxMarker.getLngLat(),
            //         zoom,
            //     });
            }
        }
    }

    private focus(clicked: Point | Line): void {
        this.unfocus();

        this.folders.folders.forEach((folder: Folder) => {
            folder.paths.forEach((path: Path) => {
                if (this._mapboxMap) {
                    if (clicked instanceof Point && path.points.includes(clicked)) {
                        if (this._focused instanceof Folder && this._focused.paths.includes(path)) {
                            this._focused = path.focus(this._mapboxMap);
                        } else if (this._focused instanceof Path && this._focused.points.includes(clicked)) {
                            this._focused = clicked.focus();
                        } else if (this._focused instanceof Path && folder.paths.includes(this._focused)) {
                            this._focused = path.focus(this._mapboxMap);
                        } else if (this._focused instanceof Point && path.points.includes(this._focused)) {
                            this._focused = clicked.focus();
                        } else {
                            this._focused = folder.focus(this._mapboxMap);
                        }
                    // } else if (clicked instanceof Line && path._line === clicked) {
                    //     if (this._focused instanceof Folder && this._focused.paths.includes(path)) {
                    //         this._focused = path.focus(this._mapboxMap);
                    //     } else if (this._focused instanceof Path) {
                    //         this._focused = path.focus(this._mapboxMap);
                    //     } else {
                    //         this._focused = folder.focus(this._mapboxMap);
                    //     }
                    }
                }
            });
        });

        this.fitBounds();
        this.autoscroll();
    }

    private unfocus(): void {
        this.folders.folders.forEach((folder: Folder) => {
            if (this._mapboxMap) {
                folder.unfocus(this._mapboxMap);
            }
        });
    }
}
