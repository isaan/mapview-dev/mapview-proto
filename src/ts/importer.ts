import { Folder } from './folder';
import * as CircularJSON from 'circular-json';
import * as Mapbox from 'mapbox-gl';
import { EXAMPLE } from './constants';
import { COLORS } from './enums';
import { ImageOverlay } from './imageOverlay';
import { Mapview } from './mapview';
import { Path } from './path';
import { Point } from './point';

export class Importer {
    public readFile(map: Mapview, file: File | null): this {
        if (file !== undefined && file !== null) {
            const FILE_READER: FileReader = new FileReader();
            const FILE_EXTENSION: string = file.name.split('.')[1];

            FILE_READER.onload = () => {
                const RESULT: string = (FILE_READER.result as string);
                switch (FILE_EXTENSION) {
                    case 'F25': {
                        if (map._mapboxMap) {
                            map.addFolder(this.readF25(map._mapboxMap, RESULT), true);
                        }
                        break;
                    }
                    case 'json': {
                        this.readJSON(map, CircularJSON.parse(RESULT));
                        break;
                    }
                    case 'png':
                    case 'jpg':
                    case 'jpeg': {
                        if (map._mapboxMap) {
                            map.images.addImage(
                                map._mapboxMap,
                                new ImageOverlay(map._mapboxMap, RESULT,
                                [
                                    map._mapboxMap.getCenter().lng - 0.005,
                                    map._mapboxMap.getCenter().lat + 0.005,
                                ], [
                                    map._mapboxMap.getCenter().lng + 0.005,
                                    map._mapboxMap.getCenter().lat - 0.005,
                                ], [
                                    map._mapboxMap.getCenter().lng + 0.005,
                                    map._mapboxMap.getCenter().lat + 0.005,
                                ],
                                true),
                            );
                        }
                        break;
                    }
                }
            };
            (FILE_EXTENSION === 'png' || FILE_EXTENSION === 'jpg' || FILE_EXTENSION === 'jpeg') ? FILE_READER.readAsDataURL(file) : FILE_READER.readAsText(file);
        }
        return this;
    }

    public readTestFile(map: Mapview): this {
        this.readJSON(map, EXAMPLE);
        return this;
    }

    private readF25(map: Mapbox.Map, result: string): Folder {
        const EQUATION: Input = {
            kind: {
                name: 'equation',
                type: 'f25',
            },
            label: 'Equation',
            value: 'd0',
        };

        const LIMIT_HIGH: Limit = {
            color: COLORS.RED,
            value: 500,
        };

        const LIMIT_LOW: Limit = {
            color: COLORS.GREEN,
            value: 150,
        };

        const PATH: Path = new Path(map, EQUATION, LIMIT_HIGH, LIMIT_LOW);
        PATH.information = [
            {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Name',
                value: '',
            }, {
                kind: {
                    name: 'date',
                    type: 'string',
                },
                label: 'Date',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Operator',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Route',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Direction',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Work part',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Climate',
                value: '',
            }, {
                kind: {
                    name: 'temperature',
                    type: 'celsius',
                },
                label: 'Temperature',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'longString',
                },
                label: 'Comment',
                value: '',
            },
        ];

        const FOLDER: Folder = new Folder(EQUATION, LIMIT_HIGH, LIMIT_LOW).addPath(PATH);
        FOLDER.information = [
            {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Name',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Client',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Folder',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Locality',
                value: '',
            }, {
                kind: {
                    name: 'date',
                    type: 'string',
                },
                label: 'Date',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'string',
                },
                label: 'Work',
                value: '',
            }, {
                kind: {
                    name: 'text',
                    type: 'longString',
                },
                label: 'Comment',
                value: '',
            },
        ];
        const LINES: string[] = result.split('\n');

        for (let i = 0; i < LINES.length; i++) {
            const SPLITTED_LINE: string[] = LINES[i].replace(/\s/g, '').split(',');
            if (SPLITTED_LINE[0] === '5280') {
                const COORD: Mapbox.LngLat = new Mapbox.LngLat(Number(SPLITTED_LINE[4]), Number(SPLITTED_LINE[3]));

                if (LINES[i + 6]) {
                    const F25VALUES: string[] = LINES[i + 6].replace(/\s/g, '').split(',');

                    const POINT: Point = new Point(COORD);
                    POINT.values = {
                        kind: 'F25Values',
                        d0: Number(F25VALUES[2]),
                        d200: Number(F25VALUES[3]),
                        d300: Number(F25VALUES[4]),
                        d450: Number(F25VALUES[5]),
                        d600: Number(F25VALUES[6]),
                        d900: Number(F25VALUES[7]),
                        d1200: Number(F25VALUES[8]),
                        d1500: Number(F25VALUES[9]),
                        d1800: Number(F25VALUES[10]),
                    };
                    POINT.information = [
                        {
                            kind: {
                                name: 'text',
                                type: 'longString',
                            },
                            label: 'Comment',
                            value: '',
                        }, {
                            kind: {
                                name: 'date',
                                type: 'string',
                            },
                            label: 'Date',
                            value: '',
                        }, {
                            kind: {
                                name: 'text',
                                type: 'string',
                            },
                            label: 'Linking',
                            value: '',
                        },
                    ];
                    PATH.addPoint(map, POINT);
                }
            }
        }
        return FOLDER;
    }

    private readJSON(map: Mapview, object: any): void {
        map.images.areVisible = object.images.areVisible;
        object.images.imageOverlays.forEach((imageOverlayObject: any) => {
            if (map._mapboxMap) {
                map.images.addImage(
                    map._mapboxMap,
                    new ImageOverlay(
                        map._mapboxMap,
                        imageOverlayObject.image,
                        imageOverlayObject.point1.coord,
                        imageOverlayObject.point3.coord,
                        imageOverlayObject.point2.coord,
                        false,
                    ),
                );
            }
        });

        map.folders.areLinesVisible = object.folders.areLinesVisible;
        map.folders.arePointsVisible = object.folders.arePointsVisible;
        map.folders.arePointsTextsVisible = object.folders.arePointsTextsVisible;
        map.folders.icon = object.folders.icon;

        object.folders.folders.forEach((folder: any) => {
            const FOLDER: Folder = new Folder(folder.equation, folder.limitHigh, folder.limitLow);
            FOLDER.areLinesVisible = folder.areLinesVisible;
            FOLDER.arePointsVisible = folder.arePointsVisible;
            FOLDER.arePointsTextsVisible = folder.arePointsTextsVisible;
            FOLDER.icon = folder.icon;
            FOLDER.information = folder.information;

            folder.paths.forEach((path: any) => {
                if (map._mapboxMap) {
                    const PATH: Path = new Path(map._mapboxMap, path.equation, path.limitHigh, path.limitLow);
                    PATH.areLinesVisible = path.areLinesVisible;
                    PATH.arePointsVisible = path.arePointsVisible;
                    PATH.arePointsTextsVisible = path.arePointsTextsVisible;
                    PATH.icon = path.icon;
                    PATH.information = path.information;

                    path.points.forEach((point: any) => {
                        const POINT: Point = new Point(point.coord);
                        POINT.arePointsVisible = point.arePointsVisible;
                        POINT.arePointsTextsVisible = point.arePointsTextsVisible;
                        POINT.icon = point.icon;
                        POINT.information = point.information;
                        POINT.values = point.values;
                        if (map._mapboxMap) {
                            PATH.addPoint(map._mapboxMap, POINT);
                        }
                    });
                    FOLDER.addPath(PATH);
                }
            });
            if (map._mapboxMap) {
                map.addFolder(FOLDER, false);
            }
        });

        if (map._mapboxMap) {
            map._mapboxMap.setCenter(object.mapCenter);
            map._mapboxMap.zoomTo(object.mapZoom);
        }
    }
}
