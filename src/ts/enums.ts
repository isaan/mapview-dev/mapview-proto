export const enum BUTTONS {
    LEFT_CLICK,
    MIDDLE_CLICK,
    RIGHT_CLICK,
}

export const enum COLORS {
    BLACK = '#0a0a0a',
    BLUE = '#3273dc',
    DARK_GREY = '#4a4a4a',
    GREEN = '#23d160',
    GREY = '#7f8c8d',
    ORANGE = '#e67e22',
    PURPLE = '#9b59b6',
    RED = '#ff3860',
    WHITE = '#f9f9f9',
    YELLOW = '#f1c40f',
}

// Sources:
// https://feathericons.com/
// https://akveo.github.io/eva-icons/#/?type=outline
export enum ICONS {
    ARCHIVE =
          '<polyline points="21 8 21 21 3 21 3 8"></polyline>'
        + '<rect x="1" y="3" width="22" height="5"></rect>'
        + '<line x1="10" y1="12" x2="14" y2="12"></line>',
    CAMERA =
          '<path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />'
        + '<circle cx="12" cy="13" r="4" />',
    CAMERA_OFF =
          '<line x1="1" y1="1" x2="23" y2="23"></line>'
        + '<path d="M21 21H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h3m3-3h6l2 3h4a2 2 0 0 1 2 2v9.34m-7.72-2.06a4 4 0 1 1-5.56-5.56"></path>',
    CHECK =
          '<polyline points="20 6 9 17 4 12"></polyline>',
    CIRCLE =
          '<circle cx="12" cy="12" r="10"></circle>',
    CIRCLE_OFF =
          '<circle cx="12" cy="12" r="10"></circle>'
        + '<line x1="1" y1="1" x2="23" y2="23"></line>'
        + '<line x1="4" y1="1" x2="26" y2="23" stroke="white"></line>',
    CLIPBOARD =
          '<path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path>'
        + '<rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect>',
    CROP =
          '<path d="M6.13 1L6 16a2 2 0 0 0 2 2h15" />'
        + '<path d="M1 6.13L16 6a2 2 0 0 1 2 2v15" />',
    DISC =
          '<circle cx="12" cy="12" r="10"></circle>'
        + '<circle cx="12" cy="12" r="3"></circle>',
    DOWNLOAD =
          '<path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path>'
        + '<polyline points="7 10 12 15 17 10"></polyline>'
        + '<line x1="12" y1="15" x2="12" y2="3"></line>',
    DOWNLOAD_CLOUD =
          '<polyline points="8 17 12 21 16 17"></polyline>'
        + '<line x1="12" y1="12" x2="12" y2="21"></line>'
        + '<path d="M20.88 18.09A5 5 0 0 0 18 9h-1.26A8 8 0 1 0 3 16.29"></path>',
    EXCEL =
          '<rect x="1" y="1" width="22" height="22" />'
        + '<line x1="2" y1="12" x2="22" y2="12"></line>'
        + '<line x1="2" y1="6" x2="22" y2="6"></line>'
        + '<line x1="2" y1="18" x2="22" y2="18"></line>'
        + '<line x1="12" y1="6" x2="12" y2="22"></line>'
        + '<line x1="6" y1="6" x2="6" y2="22"></line>'
        + '<line x1="18" y1="6" x2="18" y2="22"></line>',
    FILE_PLUS =
          '<path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path>'
        + '<polyline points="14 2 14 8 20 8"></polyline>'
        + '<line x1="12" y1="18" x2="12" y2="12"></line>'
        + '<line x1="9" y1="15" x2="15" y2="15"></line>',
    GLOBE =
          '<circle cx="12" cy="12" r="10"></circle>'
        + '<line x1="2" y1="12" x2="22" y2="12"></line>'
        + '<path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z"></path>',
    IMAGE =
          '<rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>'
        + '<circle cx="8.5" cy="8.5" r="1.5"></circle>'
        + '<polyline points="21 15 16 10 5 21"></polyline>',
    IMAGE_OFF =
          '<rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>'
        + '<circle cx="8.5" cy="8.5" r="1.5"></circle>'
        + '<polyline points="21 15 16 10 5 21"></polyline>'
        + '<line x1="1" y1="1" x2="23" y2="23"></line>'
        + '<line x1="4" y1="1" x2="26" y2="23" stroke="white"></line>',
    LIFE_BUOY =
          '<circle cx="12" cy="12" r="10"></circle>'
        + '<circle cx="12" cy="12" r="4"></circle>'
        + '<line x1="4.93" y1="4.93" x2="9.17" y2="9.17"></line>'
        + '<line x1="14.83" y1="14.83" x2="19.07" y2="19.07"></line>'
        + '<line x1="14.83" y1="9.17" x2="19.07" y2="4.93"></line>'
        + '<line x1="14.83" y1="9.17" x2="18.36" y2="5.64"></line>'
        + '<line x1="4.93" y1="19.07" x2="9.17" y2="14.83"></line>',
    MAP =
          '<polygon points="1 6 1 22 8 18 16 22 23 18 23 2 16 6 8 2 1 6"></polygon>'
        + '<line x1="8" y1="2" x2="8" y2="18"></line>'
        + '<line x1="16" y1="6" x2="16" y2="22"></line>',
    MAP_PIN =
          '<path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>'
        + '<circle cx="12" cy="10" r="3"></circle>',
    MOVE =
          '<polyline points="5 9 2 12 5 15"></polyline>'
        + '<polyline points="9 5 12 2 15 5"></polyline>'
        + '<polyline points="15 19 12 22 9 19"></polyline>'
        + '<polyline points="19 9 22 12 19 15"></polyline>'
        + '<line x1="2" y1="12" x2="22" y2="12"></line>'
        + '<line x1="12" y1="2" x2="12" y2="22"></line>',
    OCTAGON =
          '<polygon points="7.86 2 16.14 2 22 7.86 22 16.14 16.14 22 7.86 22 2 16.14 2 7.86 7.86 2"></polygon>',
    PLAY_CIRCLE =
          '<circle cx="12" cy="12" r="10"></circle>'
        + '<polygon points="10 8 16 12 10 16 10 8"></polygon>',
    SAVE =
          '<path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"></path>'
        + '<polyline points="17 21 17 13 7 13 7 21"></polyline>'
        + '<polyline points="7 3 7 8 15 8"></polyline>',
    SEARCH =
          '<circle cx="11" cy="11" r="8"></circle>'
        + '<line x1="21" y1="21" x2="16.65" y2="16.65"></line>',
    SHARE_OFF =
          '<circle cx="6" cy="5" r="3" />'
        + '<circle cx="18" cy="12" r="3" />'
        + '<circle cx="6" cy="19" r="3" />'
        + '<line x1="15.42" y1="13.51" x2="8.59" y2="17.49" />'
        + '<line x1="8.59" y1="6.51" x2="15.41" y2="10.49" />'
        + '<line x1="-1" y1="1" x2="21" y2="23"></line>'
        + '<line x1="2" y1="1" x2="24" y2="23" stroke="white"></line>',
    SLIDERS =
          '<line x1="4" y1="21" x2="4" y2="14"></line>'
        + '<line x1="4" y1="10" x2="4" y2="3"></line>'
        + '<line x1="12" y1="21" x2="12" y2="12"></line>'
        + '<line x1="12" y1="8" x2="12" y2="3"></line>'
        + '<line x1="20" y1="21" x2="20" y2="16"></line>'
        + '<line x1="20" y1="12" x2="20" y2="3"></line>'
        + '<line x1="1" y1="14" x2="7" y2="14"></line>'
        + '<line x1="9" y1="8" x2="15" y2="8"></line>'
        + '<line x1="17" y1="16" x2="23" y2="16"></line>',
    SQUARE =
          '<rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>',
    STAR =
          '<polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>',
    TARGET =
          '<circle cx="12" cy="12" r="10"></circle>'
        + '<circle cx="12" cy="12" r="6"></circle>'
        + '<circle cx="12" cy="12" r="2"></circle>',
    TEXT_OFF =
          '<polyline points="4 7 4 4 20 4 20 7"></polyline>'
        + '<line x1="9" y1="20" x2="15" y2="20"></line>'
        + '<line x1="12" y1="4" x2="12" y2="20"></line>'
        + '<line x1="1" y1="1" x2="23" y2="23"></line>'
        + '<line x1="4" y1="1" x2="26" y2="23" stroke="white"></line>',
    TOGGLE =
          '<rect x="1" y="5" width="22" height="14" rx="7" ry="7"></rect>'
        + '<circle cx="8" cy="12" r="3"></circle>',
    TRASH =
          '<polyline points="3 6 5 6 21 6"></polyline>'
        + '<path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>'
        + '<line x1="10" y1="11" x2="10" y2="17"></line>'
        + '<line x1="14" y1="11" x2="14" y2="17"></line>',
    TRIANGLE =
          '<path d="M10.29 3.86L1.82 18a2 2 0 0 0 1.71 3h16.94a2 2 0 0 0 1.71-3L13.71 3.86a2 2 0 0 0-3.42 0z"></path>',
    X =
          '<line x1="18" y1="6" x2="6" y2="18" />'
        + '<line x1="6" y1="6" x2="18" y2="18" />',
    X_CIRCLE =
          '<circle cx="12" cy="12" r="10"></circle>'
        + '<line x1="15" y1="9" x2="9" y2="15"></line>'
        + '<line x1="9" y1="9" x2="15" y2="15"></line>',
    ZOOM_IN =
          '<circle cx="11" cy="11" r="8"></circle>'
        + '<line x1="21" y1="21" x2="16.65" y2="16.65"></line>'
        + '<line x1="11" y1="8" x2="11" y2="14"></line>'
        + '<line x1="8" y1="11" x2="14" y2="11"></line>',
    ZOOM_OUT =
          '<circle cx="11" cy="11" r="8"></circle>'
        + '<line x1="21" y1="21" x2="16.65" y2="16.65"></line>'
        + '<line x1="8" y1="11" x2="14" y2="11"></line>',
}
export enum MARKER_ICONS {
    CIRCLE = ICONS.CIRCLE,
    DISC = ICONS.DISC,
    LIFE_BUOY = ICONS.LIFE_BUOY,
    OCTAGON = ICONS.OCTAGON,
    SQUARE = ICONS.SQUARE,
    STAR = ICONS.STAR,
    TARGET = ICONS.TARGET,
    TRIANGLE = ICONS.TRIANGLE,
    X_CIRCLE = ICONS.X_CIRCLE,
}

// Sources:
// https://www.mapbox.com/api-documentation/#styles
export enum STYLES {
    STREETS = 'mapbox://styles/mapbox/streets-v10',
    // BASIC = 'mapbox://styles/mapbox/basic-v9',
    LIGHT = 'mapbox://styles/mapbox/light-v9',
    DARK = 'mapbox://styles/mapbox/dark-v9',
    SATELLITE = 'mapbox://styles/mapbox/satellite-streets-v10',
}

// export const enum UNITS {
//     TEMPERATURE = {
//         celsius = '°C',
//         fahrenheit = '°F',
//     },
// }
