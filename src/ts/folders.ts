import * as Mapbox from 'mapbox-gl';
import { MARKER_ICONS } from './enums';
import { Folder } from './folder';
import { Line } from './line';
import { Path } from './path';
import { Point } from './point';

export class Folders {
    public readonly _name: string = 'Folders';
    public areLinesVisible: boolean = true;
    public arePointsVisible: boolean = true;
    public arePointsTextsVisible: boolean = true;
    public folders: Folder[] = new Array();
    public icon: string = Object.keys(MARKER_ICONS)[0];

    // ---
    // PUBLIC
    // ---

    public addFolder(map: Mapbox.Map, folder: Folder): this {
        if (! this.areLinesVisible) {
            folder.toggleLines(map);
        }
        if (! this.arePointsVisible) {
            folder.togglePoints();
        }
        if (! this.arePointsTextsVisible) {
            folder.togglePointsText();
        }

        this.folders.push(folder
            .updateIcon(this.icon)
            .updateColors(map));

        return this;
    }

    public clean(map: Mapbox.Map): this {
        this.folders.forEach((folder: Folder) => {
            folder.clean(map);
        });
        return this;
    }

    public init(map: Mapbox.Map): this {
        map.on('style.load', () => {
            this.folders.forEach((folder: Folder) => {
                folder.paths.forEach((path: Path) => {
                    map.addLayer(path._line.genLayer());
                });
            });
        });
        return this;
    }

    public toggleLines(map: Mapbox.Map): this {
        this.areLinesVisible = ! this.areLinesVisible;
        this.folders.forEach((folder: Folder) => {
            folder.toggleLines(map, this.areLinesVisible);
        });
        return this;
    }

    public togglePoints(): this {
        this.arePointsVisible = ! this.arePointsVisible;
        this.folders.forEach((folder: Folder) => {
            folder.togglePoints(this.arePointsVisible);
        });
        return this;
    }

    public togglePointsText(): this {
        this.arePointsTextsVisible = ! this.arePointsTextsVisible;
        this.folders.forEach((folder: Folder) => {
            folder.togglePointsText(this.arePointsTextsVisible);
        });
        return this;
    }

    public updateIcon(icon: string): this {
        this.folders.forEach((folder: Folder) => {
            folder.updateIcon(icon);
        });
        this.icon = icon;
        return this;
    }
}
