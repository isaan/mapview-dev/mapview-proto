export class Checker {
    public updateInput(input: Input, value: string): boolean {
        let regex: string = '^$';
        switch (input.kind.name) {
            case 'equation': {
                switch (input.kind.type) {
                    case 'f25': {
                        regex = '^(d0|d200|d300|d450|d600|d900|d1200|d1500|d1800)( ?(\\+|-|\\*|\\/) ?(d0|d200|d300|d450|d600|d900|d1200|d1500|d1800|[0-9]+))*$';
                        break;
                    }
                }
                break;
            }
            case 'temperature': {
                switch (input.kind.type) {
                    case 'celsius': {
                        regex = '(-)?[0-9]{1-2}';
                        break;
                    }
                }
                break;
            }
        }
        return RegExp(regex).test(value);
    }
}
