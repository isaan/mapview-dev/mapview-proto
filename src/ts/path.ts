import * as Mapbox from 'mapbox-gl';
import { Line } from './line';
import { Point } from './point';

export class Path {
    public readonly _name: string = 'Path';
    public _line: Line;
    public areLinesVisible: boolean = true;
    public arePointsVisible: boolean = true;
    public arePointsTextsVisible: boolean = true;
    public equation: Input;
    public icon: string = '';
    public information: Input[] = new Array();
    public limitHigh: Limit;
    public limitLow: Limit;
    public points: Point[] = new Array();

    constructor(map: Mapbox.Map, equation: Input, limitHigh: Limit, limitLow: Limit) {
        this._line = new Line(map, this.points);
        this.equation = { ...equation };
        this.limitHigh = { ...limitHigh }; // Copy object
        this.limitLow = { ...limitLow };
    }

    public addPoint(map: Mapbox.Map, point: Point): this {
        point._icon.setText(String(this.points.length + 1));
        point._mapboxMarker.on('drag', () => {
            this._line.update(map, this.limitHigh, this.limitLow);
        });
        this.points.push(point);
        return this;
    }

    public focus(map: Mapbox.Map): this {
        this.points.forEach((point: Point) => {
            point.focus();
        });
        this._line.focus(map);
        return this;
    }

    public toggleLines(map: Mapbox.Map, parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.areLinesVisible)) {
            this.areLinesVisible = ! this.areLinesVisible;
            if (map.getLayer(this._line._id)) {
                map.removeLayer(this._line._id);
                map.removeSource(this._line._id);
            } else {
                map.addLayer(this._line.genLayer());
            }
        }
        return this;
    }

    public togglePoints(parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.arePointsVisible)) {
            this.arePointsVisible = ! this.arePointsVisible;
            this.points.forEach((point: Point) => {
                point.togglePoints(this.arePointsVisible);
            });
        }
        return this;
    }

    public togglePointsText(parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.arePointsTextsVisible)) {
            this.arePointsTextsVisible = ! this.arePointsTextsVisible;
            this.points.forEach((point: Point) => {
                point.togglePointsText(this.arePointsTextsVisible);
            });
        }
        return this;
    }

    public unfocus(map: Mapbox.Map): this {
        this.points.forEach((point: Point) => {
            point.unfocus();
        });
        this._line.unfocus(map);
        return this;
    }

    public updateColors(map: Mapbox.Map, parentsEquation?: Input, parentsLimitHigh?: Limit, parentsLimitLow?: Limit): this {
        if (parentsEquation && parentsLimitHigh && parentsLimitLow) {
            this.equation = { ...parentsEquation };
            this.limitHigh = { ...parentsLimitHigh };
            this.limitLow = { ...parentsLimitLow };
        }

        this.points.forEach((point: Point) => {
            point.updateCurrentValue(String(this.equation.value));
            point.updateColor(this.limitHigh, this.limitLow);
        });
        this._line.update(map, this.limitHigh, this.limitLow);
        return this;
    }

    public updateIcon(icon: string): this {
        this.icon = icon;
        this.points.forEach((point: Point) => {
            point.updateIcon(icon);
        });
        return this;
    }
}
