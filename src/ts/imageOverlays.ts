import * as Mapbox from 'mapbox-gl';
import { ImageOverlay } from './imageOverlay';

export class ImageOverlays {
    public areVisible: boolean = true;
    public imageOverlays: ImageOverlay[] = new Array();

    public addImage(map: Mapbox.Map, imageOverlay: ImageOverlay): this {
        this.imageOverlays.push(imageOverlay);
        return this;
    }

    public clean(map: Mapbox.Map): this {
        this.imageOverlays.forEach((imageOverlay: ImageOverlay) => {
            imageOverlay.clean(map);
        });
        this.imageOverlays = new Array();

        return this;
    }

    public init(map: Mapbox.Map): this {
        map.on('style.load', () => {
            this.imageOverlays.forEach((imageOverlay: ImageOverlay) => {
                map.addLayer(imageOverlay.genLayer());
            });
        });
        return this;
    }

    public toggle(map: Mapbox.Map): this {
        this.areVisible = ! this.areVisible;
        this.imageOverlays.forEach((image: ImageOverlay) => {
            image.toggle(map);
        });
        return this;
    }

    public updateOpacity(map: Mapbox.Map): this {
        this.imageOverlays.forEach((imageOverlay: ImageOverlay) => {
            map.setPaintProperty(imageOverlay._id, 'raster-opacity', imageOverlay.opacity);
        });
        return this;
    }
}
