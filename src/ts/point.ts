import * as Mapbox from 'mapbox-gl';
import * as MathJS from 'mathjs';
import { COLORS, MARKER_ICONS } from './enums';
import { Blender } from './blender';
import { Icon } from './icon';

export class Point {
    public readonly _name: string = 'Point';
    public _currentValue: number = 0;
    public _mapboxMarker: Mapbox.Marker;
    public _icon: Icon = new Icon()
        .setFill(COLORS.WHITE)
        .setIcon(Object.keys(MARKER_ICONS)[0])
        .setStrokeWidth(3);
    public arePointsVisible: boolean = true;
    public arePointsTextsVisible: boolean = true;
    public coord: Mapbox.LngLatLike;
    public icon: string = Object.keys(MARKER_ICONS)[0];
    public information: Input[] = new Array();
    public values: Values;

    constructor(coord: Mapbox.LngLatLike) {
        this._mapboxMarker = new Mapbox.Marker({
            element: (this._icon.svg as unknown as HTMLElement),
        }).setLngLat(coord);
        this.coord = this._mapboxMarker.getLngLat();

        this._mapboxMarker.on('dragend', () => {
            this.coord = this._mapboxMarker.getLngLat();
            this._mapboxMarker.setDraggable(false);
        });
    }

    public focus(): this {
        this._icon.setSize(28);
        return this;
    }

    public togglePoints(parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.arePointsVisible)) {
            this.arePointsVisible = ! this.arePointsVisible;
            this._icon.toggle();
        }
        return this;
    }

    public togglePointsText(parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.arePointsTextsVisible)) {
            this.arePointsTextsVisible = ! this.arePointsTextsVisible;
            this._icon.toggleText();
        }
        return this;
    }

    public unfocus(): this {
        this._icon.setSize(20);
        return this;
    }

    public updateColor(limitHigh: Limit, limitLow: Limit): this {
        let color: string;

        if (this._currentValue > limitLow.value) {
            if (this._currentValue > limitHigh.value) {
                color = limitHigh.color;
            } else {
                color = new Blender().blend(limitLow.color, limitHigh.color, 0.5);
            }
        } else {
            color = limitLow.color;
        }
        this._icon.setFill(color);

        return this;
    }

    public updateCurrentValue(equation: string): this {
        if (this.values) {
            const PARSER: MathJS.Parser = MathJS.parser();

            switch (this.values.kind) {
                case 'F25Values': {
                    PARSER.eval('d0 = '    + String(this.values.d0));
                    PARSER.eval('d200 = '  + String(this.values.d200));
                    PARSER.eval('d300 = '  + String(this.values.d300));
                    PARSER.eval('d450 = '  + String(this.values.d450));
                    PARSER.eval('d600 = '  + String(this.values.d600));
                    PARSER.eval('d900 = '  + String(this.values.d900));
                    PARSER.eval('d1200 = ' + String(this.values.d1200));
                    PARSER.eval('d1500 = ' + String(this.values.d1500));
                    PARSER.eval('d1800 = ' + String(this.values.d1800));
                    break;
                }
            }
            this._currentValue = PARSER.eval(equation);
        }
        return this;
    }

    public updateIcon(icon: string): this {
        this.icon = icon;
        this._icon.setIcon(icon);
        return this;
    }
}
