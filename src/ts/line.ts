import * as Mapbox from 'mapbox-gl';
import { COLORS } from './enums';
import { Blender } from './blender';
import { Point } from './point';
import { Geometry, GeoJsonProperties, FeatureCollection } from 'geojson';

export class Line {
    public _data: object = {
        type: 'FeatureCollection',
        features: [],
    };
    public _id: string;
    public _markers: Point[];

    constructor(map: Mapbox.Map, points: Point[]) {
        this._id = String(Math.random());
        this._markers = points;
        map.addLayer(this.genLayer());

        map.on('mouseenter', this._id, () => {
            map.getCanvas().style.cursor = 'pointer';
        });

        map.on('mouseleave', this._id, () => {
            map.getCanvas().style.cursor = '';
        });
    }

    // ---
    // PUBLIC
    // ---

    public focus(map: Mapbox.Map): this {
        map.setPaintProperty(this._id, 'line-width', 6);
        return this;
    }

    public genLayer(): Mapbox.Layer {
        return {
            id: this._id,
            layout: {
                'line-join': 'round',
                'line-cap': 'round',
            },
            paint: {
                'line-color': ['get', 'color'],
                'line-width': 6,
            },
            source: ({
                type: 'geojson',
                data: this._data,
            } as object as Mapbox.GeoJSONSource),
            type: 'line',
        };
    }

    public unfocus(map: Mapbox.Map): this {
        map.setPaintProperty(this._id, 'line-width', 4);
        return this;
    }

    public update(map: Mapbox.Map, limitHigh: Limit, limitLow: Limit): this {
        (this._data as any).features = [];

        for (let index = 1; index < this._markers.length; index++) {
            let color: string = COLORS.WHITE;
            if (limitHigh && limitLow) {
                color = this.calculateColor(limitHigh, limitLow, index);
            }

            (this._data as any).features.push({
                type: 'Feature',
                properties: {
                    color,
                },
                geometry: {
                    type: 'LineString',
                    coordinates: [
                        [ this._markers[index - 1]._mapboxMarker.getLngLat().lng,
                        this._markers[index - 1]._mapboxMarker.getLngLat().lat ],
                        [ this._markers[index]._mapboxMarker.getLngLat().lng,
                        this._markers[index]._mapboxMarker.getLngLat().lat ],
                    ],
                },
            });
        }

        if (map.getSource(this._id) as Mapbox.GeoJSONSource) {
            (map.getSource(this._id) as Mapbox.GeoJSONSource).setData(this._data as FeatureCollection<Geometry, GeoJsonProperties>);
        }

        return this;
    }

    // ---
    // PRIVATE
    // ---

    private calculateColor(limitHigh: Limit, limitLow: Limit, index: number): string {
        const VALUE: number = (this._markers[index - 1]._currentValue + this._markers[index]._currentValue) / 2;
        let color: string;

        if (VALUE > limitLow.value) {
            if (VALUE > limitHigh.value) {
                color = limitHigh.color;
            } else {
                color = new Blender().blend(limitLow.color, limitHigh.color, 0.5);
            }
        } else {
            color = limitLow.color;
        }
        return color;
    }
}
