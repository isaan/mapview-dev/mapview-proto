import * as Mapbox from 'mapbox-gl';
import { Point } from './point';
import { Path } from './path';

export class Folder {
    public readonly _name: string = 'Folder';
    public areLinesVisible: boolean = true;
    public arePointsVisible: boolean = true;
    public arePointsTextsVisible: boolean = true;
    public equation: Input;
    public icon: string = '';
    public information: Input[] = new Array();
    public limitHigh: Limit;
    public limitLow: Limit;
    public paths: Path[] = new Array();

    constructor(equation: Input, limitHigh: Limit, limitLow: Limit) {
        this.equation = { ...equation };
        this.limitHigh = { ...limitHigh };
        this.limitLow = { ...limitLow };
    }

    public addPath(path: Path): this {
        this.paths.push(path);
        return this;
    }

    public clean(map: Mapbox.Map): this {
        this.paths.forEach((path: Path) => {
            path.points.forEach((point: Point) => {
                point._mapboxMarker.remove();
            });
            map.removeLayer(path._line._id);
            map.removeSource(path._line._id);
        });
        this.paths = new Array();

        return this;
    }

    public focus(map: Mapbox.Map): this {
        this.paths.forEach((path: Path) => {
            path.focus(map);
        });
        return this;
    }

    public toggleLines(map: Mapbox.Map, parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.areLinesVisible)) {
            this.areLinesVisible = ! this.areLinesVisible;
            this.paths.forEach((path: Path) => {
                path.toggleLines(map, this.areLinesVisible);
            });
        }
        return this;
    }

    public togglePoints(parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.arePointsVisible)) {
            this.arePointsVisible = ! this.arePointsVisible;
            this.paths.forEach((path: Path) => {
                path.togglePoints(this.arePointsVisible);
            });
        }
        return this;
    }

    public togglePointsText(parentsStatus?: boolean): this {
        if (parentsStatus === undefined || (parentsStatus !== undefined && parentsStatus !== this.arePointsTextsVisible)) {
            this.arePointsTextsVisible = ! this.arePointsTextsVisible;
            this.paths.forEach((path: Path) => {
                path.togglePointsText(this.arePointsTextsVisible);
            });
        }
        return this;
    }

    public unfocus(map: Mapbox.Map): this {
        this.paths.forEach((path: Path) => {
            path.unfocus(map);
        });
        return this;
    }

    public updateColors(map: Mapbox.Map): this {
        this.paths.forEach((path: Path) => {
            path.updateColors(map, this.equation, this.limitHigh, this.limitLow);
        });
        return this;
    }

    public updateIcon(icon: string): this {
        this.icon = icon;
        this.paths.forEach((path: Path) => {
            path.updateIcon(icon);
        });
        return this;
    }
}
