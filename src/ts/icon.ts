import { BROWSER } from './constants';
import { ICONS } from './enums';

export class Icon {
    public svg: SVGSVGElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    public text: SVGTextElement = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    public textOutline: SVGTextElement = document.createElementNS('http://www.w3.org/2000/svg', 'text');

    constructor() {
        this.svg.setAttribute('fill', 'none');
        this.svg.setAttribute('height', '24');
        this.svg.setAttribute('stroke', 'currentColor');
        this.svg.setAttribute('stroke-width', '2');
        this.svg.setAttribute('stroke-linecap', 'round');
        this.svg.setAttribute('stroke-linejoin', 'round');
        this.svg.setAttribute('viewBox', '0 0 24 24');
        this.svg.setAttribute('width', '24');

        let y: string;
        (BROWSER === 'Chrome' || BROWSER === 'Safari') ? y = '12' : y = '16';

        this.textOutline.setAttribute('alignment-baseline', 'central');
        this.textOutline.setAttribute('stroke-width', '4');
        this.textOutline.setAttribute('text-anchor', 'middle');
        this.textOutline.setAttribute('x', '12');
        this.textOutline.setAttribute('y', y);

        this.text.setAttribute('alignment-baseline', 'central');
        this.text.setAttribute('stroke-width', '1');
        this.text.setAttribute('text-anchor', 'middle');
        this.text.setAttribute('x', '12');
        this.text.setAttribute('y', y);
    }

    // ---
    // PUBLIC
    // ---

    public setFill(color: string): this {
        if (color) {
            this.svg.setAttribute('fill', color);
            this.textOutline.setAttribute('stroke', color);
            this.textOutline.setAttribute('fill', color);
        }
        return this;
    }

    public setIcon(icon: string): this {
        if (icon) {
            this.svg.innerHTML = ICONS[(icon as any)];
            this.svg.appendChild(this.textOutline);
            this.svg.appendChild(this.text);
        }
        return this;
    }

    public setText(string: string): this {
        this.textOutline.innerHTML = string;
        this.svg.appendChild(this.textOutline);
        this.text.innerHTML = string;
        this.svg.appendChild(this.text);
        return this;
    }

    public setSize(size: number): this {
        if (size) {
            this.svg.setAttribute('width', String(size));
            this.svg.setAttribute('height', String(size));
        }
        return this;
    }

    public setStroke(color: string): this {
        if (color) {
            this.svg.setAttribute('stroke', color);
            this.text.setAttribute('stroke', color);
            this.text.setAttribute('fill', color);
        }
        return this;
    }

    public setStrokeWidth(size: number): this {
        if (size) {
            this.svg.setAttribute('stroke-width', String(size));
        }
        return this;
    }

    public toggle(): this {
        this.svg.classList.toggle('is-hidden');
        return this;
    }

    public toggleText(): this {
        this.text.classList.toggle('is-hidden');
        this.textOutline.classList.toggle('is-hidden');
        return this;
    }

    public toString(): string {
        return this.svg.outerHTML;
    }
}
