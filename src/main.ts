import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';

import Buefy from 'buefy';
Vue.use(Buefy);

import 'buefy/dist/buefy.css';
import 'bulma/css/bulma.css';
import 'mapbox-gl/dist/mapbox-gl.css';

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
