import Vue from 'vue';
import Vuex from 'vuex';

import { Mapview } from '@/ts/mapview';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        map: new Mapview(),
    },
});
